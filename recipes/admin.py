from django.contrib import admin
from recipes.models import Recipe, RecipeStep, Ingredient
# Register your models here.
@admin.register(Recipe)
class RecipeAdmin(admin.ModelAdmin):
    list_display = (
        'title',
        'id',
        'description'
    )


@admin.register(RecipeStep)
class RecipeStepAdmin(admin.ModelAdmin):
    list_display = (
        'step_number',
        'instruction',
        'id',
        'recipe_title'
    )    


@admin.register(Ingredient)
class IngredientAdmin(admin.ModelAdmin):
    list_display = (
        'amount',
        'food_item',
        'id',
    )